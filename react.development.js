const { useState, useEffect } = React;

/**
 * The app component, utilizing both components (MenuItems and DetailsView) to display the website
 */
const App = () => {
    const [searchTerm, setSearchTerm] = useState('');
    const [searchResults, setSearchResults] = useState([]);
    const [currentArt, setCurrentArt] = useState(null);
    const [artExtraInfo, setArtExtraInfo] = useState(null);
    const [error, setError] = useState(null);

    /**
     * Fetches artwork information by ID from the Art Institute of Chicago API.
     * @param {number} artworkId - The ID of the artwork to fetch.
     */
    const fetchArtById = (artworkId) => {
        fetch(`https://api.artic.edu/api/v1/artworks/${artworkId}`)
            .then((response) => response.json())
            .then((r) => {
                setArtExtraInfo(r);
            })
            .catch((error) => {
                setError(error);
            });
    };

    /**
     * Checks if a given artwork ID matches the ID of the currently selected artwork.
     * @param {number} artworkId - The ID of the artwork to check against the current selection.
     * @returns {boolean} True if the provided artwork ID matches the ID of the currently selected artwork; otherwise, false.
     */
    const isSelected = (artworkId) => {
        return currentArt && currentArt.id === artworkId;
    };

    /**
     * Sets the currently selected artwork and fetches additional information based on the selected artwork ID.
     * @param {Object} artwork - The artwork object representing the selected artwork.
     */
    const handleItemClick = (artwork) => {
        setCurrentArt(artwork);
        fetchArtById(artwork.id);
    };

    /**
     * Handles the search action. Will be given an array of artworks based on the search.
     * @param {Event} e - The event object from the search action.
     */ 
    const handleSearch = (e) => {
        e.preventDefault();
        fetch(`https://api.artic.edu/api/v1/artworks/search?q=${encodeURIComponent(searchTerm)}`).then((r) => r.json()).then((r) => {
                    setSearchResults(r.data);
                    setError(r.length == 0 ? "No data found." : null);
                }).catch((r) => {
                    setError(r);
                });
    };

    /**
     * Component that formats text into paragraphs based on line breaks.
     * @param {string} textTobeFormatted - Text to be formatted into paragraphs.
     * @returns {JSX.Element | null} Formatted HTML element or null if no text is provided.
     */
    const TextSpace = ({ textTobeFormatted }) => {
        if (!textTobeFormatted) {
          return null;
        }
      
        const paragraphs = textTobeFormatted.split("\n").map((line, index) => (
          <p key={index} className={"formattedP"}>
            {line}
          </p>
        ));
      
        return <div>{paragraphs}</div>;
    };

    /**
     * A component to add artwork description.
     * @param {string|null} textTobeFormatted - The description text to be displayed.
     * @returns {JSX.Element} - Component displaying artwork description.
     */
    const AddDescription = ({ textTobeFormatted }) => {
        if(textTobeFormatted === null){
            return <div>There is no description to be added here.</div>
        }
        
        return <div dangerouslySetInnerHTML={{__html: artExtraInfo && artExtraInfo.data.description}}></div>
    }

    return [
        <MenuItem searchTerm={searchTerm} setSearchTerm={setSearchTerm} error={error} setError={setError} searchResults={searchResults} isSelected={isSelected} handleItemClick={handleItemClick} handleSearch={handleSearch}/>,
        <DetailsView currentArt={currentArt} artExtraInfo={artExtraInfo} TextSpace={TextSpace} AddDescription={AddDescription}/>
    ];
}

/**
 * MenuItem component (left side) for the Art Institute of Chicago search.
 * @param {function} setSearchTerm - Function to set the search term.
 * @param {string|null} error - Error message, if any.
 * @param {Array<Object>} searchResults - List of search results (artworks).
 * @param {function} isSelected - Function to check if an item is selected.
 * @param {function} handleItemClick - Function to handle item click.
 * @param {function} handleSearch - Function to handle search action.
 * @returns {JSX.Element} - Header component for search functionality.
 */
const MenuItem = ({setSearchTerm, error, searchResults, isSelected, handleItemClick, handleSearch}) => {    
    return (
        <header>
            <h1>Art Institute of Chicago Search</h1>
            <form onSubmit={handleSearch}>
                <input type="text" onChange={(e) => setSearchTerm(e.target.value)} />
                <button type="submit">Search</button>
            </form>
            <ul>
                <div>
                    {error && <div>{error}</div>}
                    {searchResults && searchResults.map((artwork) => (
                            <li className={`listElements ${isSelected(artwork.id) ? 'selected' : ''}`}
                                onClick={() => handleItemClick(artwork)}>{artwork.title}
                            </li>
                        ))}
                </div>
            </ul>
        </header>
    );
};

/**
 * DetailsView component displaying information about the selected artwork.
 * @param {Object} currentArt - Information about the currently selected artwork.
 * @param {Object} artExtraInfo - Additional information about the artwork.
 * @param {JSX.Element} TextSpace - Component to display formatted text.
 * @param {JSX.Element} AddDescription - Component to display artwork description.
 * @returns {JSX.Element} - Main component displaying artwork details.
 */
const DetailsView = ({currentArt, artExtraInfo, TextSpace, AddDescription}) => {

    /**
     * this will display the h2 if the user have no selected an artwork to display.
     */
    if(!currentArt){
        return (
        <main>
            <h2>Please select an item!</h2>
        </main>);
    }

    return (
        <main>
            <h2>{currentArt && currentArt.title}</h2>
            <div class="artist-info">
                <TextSpace textTobeFormatted={artExtraInfo && artExtraInfo.data.artist_display}/>
            </div>
            <img src={artExtraInfo && `${artExtraInfo.config.iiif_url}/${artExtraInfo.data.image_id}/full/843,/0/default.jpg`} alt={currentArt.thumbnail && currentArt.thumbnail.alt_text}/>
            <AddDescription textTobeFormatted={artExtraInfo && artExtraInfo.data.description}/>
            
            <dl>
                <dt>Medium:</dt>
                    <dd>{artExtraInfo && artExtraInfo.data.medium_display}</dd>
                <dt>Dimension:</dt>
                    <dd>{artExtraInfo && artExtraInfo.data.dimensions}</dd>
            </dl>
        </main>
    )
}

ReactDOM.render(<App/>, document.querySelector(".wrapper"));